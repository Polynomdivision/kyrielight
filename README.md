# kyrielight

Mirror your Discord server locally, so that you can use Discord using your
favourite IRC client.

## About
This project is a clone of [discordIRCd](https://github.com/creesch/discordIRCd).
It lacked some features I really wanted, like loading previous messages that
were sent in a particular channel.

So I forked it and added that feature. I, however, found out that maintaing
a fork of discordIRCd would be difficult, because
- the code was written in a difficult and cramped way,
- not many comments were written,
- and typos happend often and no one would correct me.

So I decided to port that project to TypeScript to improve the safety and
documentation of the code. I also tried to make everything as cleanly as
possible and distribute code across different files instead of one big
monolith.

## Features
- Load the Discord Token from a password store to not store it in plaintext. Gopass is currently the only password store which is supported
- Send messages to your friends from any channel using "bot commands"

## Config
### Gopass support
To load the token from the gopass password store, you just need to set the option `discord.tokenStore` to
`gopass:<Your path>`. If you can access the secret using `gopass my/secret/secret`, then you need to set
`tokenStore` to `gopass:my/secret/secret`.

**NOTE**: The `gopass` command needs to be in your path.

## Building
Run `npm install` to install dependencies and build using `npm run build`.

## Running
Run kyrielight using `npm start`.

## Why kyrielight?
Everyone likes Mash Kyrielight, right?

## License
```
MIT License

Copyright (c) 2018 Alexander 'Polynomdivision'

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
