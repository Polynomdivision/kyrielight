function genUUIDProto(): () => number {
    let counter = 0;
    return () => {
        return counter++;
    }
}

export { genUUIDProto };
