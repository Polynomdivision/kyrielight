import { readFileSync } from "fs";
import { spawnSync } from "child_process";
import { exit } from "process";
import { homedir } from "os";
import * as path from "path";

import * as Winston from "winston";

import * as Discord from "discord.js";

import { notify } from "node-notifier";

import { genUUIDProto } from "./uuid";
const genUUID = genUUIDProto();

import {
    MarkedSocket, IIRCCommand, IClientMetadata, IIRCClient, parseMessage,
    ircNickname, IRC_MAX_MESSAGE_LENGTH, IChannel, sendToIRC, isClientInChannel,
    sendBotMessage, channelNameToID, clientFromUUID
} from "./irc";

import { joinCommand } from "./cmd/join";
import { listCommand } from "./cmd/list";
import { pingCommand } from "./cmd/ping";
import { privmsgCommand } from "./cmd/privmsg";
import { partCommand } from "./cmd/part";
import { quitCommand } from "./cmd/quit";
import { userCommand } from "./cmd/user";

import { joinMax } from "./string";

import { isStatusValid, discordMessageToIRC } from "./discord";

import { Config, IFunctionParams } from "./cmd/types";


const configPath = path.join(homedir(), ".config", "kyrielight", "config.json");
let configuration: Config;
try {
    configuration = JSON.parse(readFileSync(configPath, {
        // We need to specify the encoding, or TypeScript will assume that
        // readFileSync returns a Buffer instead of a String.
        encoding: "UTF8"
    }));
} catch (e) {
    console.log(`Could not find configuration file "${configPath}"`);
    exit(1);
}

let ircClients: { [key: number]: IIRCClient } = [];
let discordClient = new Discord.Client({
    fetchAllMembers: true,
    sync: true,
});

const logger = Winston.createLogger({
    format: Winston.format.cli(),
    transports: [
        new Winston.transports.Console({
            level: configuration.debug ? "debug" : "info",
        })
    ],
});

// For simplicity during extraction
// TODO: REMOVE
function sendToIRCw(discordID: string, line: string, ircID: number = 0) {
    sendToIRC(discordID, logger, ircClients, line, ircID);
}
function sendBotMessagew(metadata: IClientMetadata, message: string) {
    sendBotMessage(metadata, ircClients, logger, message);
}

// Use "tls" or "net", depending on the settings
// TODO: There has to be a better way
const useTLS = configuration.irc.tls && configuration.irc.tls.enabled;
const netOpts = useTLS ? {
    key: readFileSync(configuration.irc.tls.key),
    cert: readFileSync(configuration.irc.tls.cert),
} : {};
const net = useTLS ? require("tls") : require("net");

if (useTLS)
    logger.info("Starting kyrielight with TLS");
else
    logger.warn("Starting kyrielight without TLS");

// Allow us to load the secret from a password store
if (configuration.discord.tokenStore.startsWith("gopass:")) {
    logger.debug("Retrieving the token from gopass");
    const { tokenStore } = configuration.discord;
    const path = tokenStore.slice("gopass:".length);
    logger.debug(`Store path: "${path}"`);

    // TODO: I feel like this could be dangerous
    const proc = spawnSync("gopass", [path], {
        encoding: "utf8",
    });

    if (proc.status !== 0) {
        logger.error("Failed to get Discord Token from gopass store");
        logger.error(`stdout: ${proc.stdout}`);
        logger.error(`stderr: ${proc.stderr}`);
        exit(1);
    }

    // Just the first line
    configuration.discord.token = proc.stdout.split("\r\n")[0];
    logger.debug(`Using token: "${configuration.discord.token}"`);
}

// Login
discordClient.login(configuration.discord.token);

// Tell the clients when something happens
discordClient.on("error", (err) => {
    sendGeneralNotice("A Discord Error occured!");
    logger.error(`[Discord.js] An error occured`);
    logger.error(err);
});

// Tell the clients when we...
// - lost the connection
// - are trying to reconnect
discordClient.on("disconnect", (event) => {
    sendGeneralNotice("Disconnected from Discord.");
    logger.warn(`[Discord.js] Disconnected!`);
    logger.warn(event);
});
discordClient.on("reconnecting", () => {
    sendGeneralNotice("Reconnecting to Discord.");
    logger.info("[Discord.js] Attempting to reconnect");
});

// Message handler
discordClient.on("message", (message) => {
    // Tell the clients that a private message has been received
    if (message.channel.type === "dm") {
        logger.debug(`Received a DM from ${message.author.username}`);
        logger.debug(`"${message.content}"`);

        // We don't need to check if any clients are connected, because
        // forEach does nothing in case the array is empty.
        Object.values(ircClients).forEach((client) => {
            const authorName = message.member ?
                message.member.displayName : message.author.username;
            const { bot, discriminator } = message.author;
            const authorIrcName = ircNickname(authorName, discriminator, bot);

            // Exclude messages written by ourselves
            if (authorIrcName === client.metadata.iDisplayName) return;

            const messageText = discordMessageToIRC(ircClients, discordClient, client.metadata, logger, message);

            /*sendBotMessagew(client.metadata,
                `Message from ${authorIrcName}`);
            sendBotMessagew(client.metadata,
                            `${messageText}`);*/

            sendBotMessage(client.metadata,
                ircClients,
                logger,
                `Message from ${authorIrcName}:`);
            sendBotMessage(client.metadata,
                ircClients,
                logger,
                `${messageText}`);

            // Only show notifications when the user wants to
            if (configuration.notify)
                notify({
                    title: authorIrcName,
                    message: messageText,
                });

        });
        return;
    }

    if (message.channel.type !== "text") return;

    const channel = message.channel as Discord.TextChannel;
    const activeClients = clientsInChannel(`#${channel.name}`);

    logger.debug(`Received a message from ${message.author.username}`);

    // We don't need to check if any clients are connected, because
    // forEach does nothing in case the array is empty.
    activeClients.forEach((client) => {
        // Was the message sent on the correct server?
        if (message.guild.id !== client.metadata.discordID) return;

        const authorName = message.member ?
            message.member.displayName : message.author.username;
        const { bot, discriminator } = message.author;
        const authorIrcName = ircNickname(authorName, discriminator, bot);

        // Check if this isn't one of our messages by "accident".
        // (1) Find the client whoose IRC nickname matches
        //     the calculated IRC nickname
        // (2) If the client is found, remove the message and perform
        //     a "continue;", altough we're in an arrow function
        const selfMessageCheck = Object.values(ircClients)
            .find((client) => {
                return client.metadata.iDisplayName === authorIrcName;
            });

        if (selfMessageCheck) {
            const msgIndex = selfMessageCheck.metadata.lastMessages
                .findIndex(msg => msg === message.content);

            if (msgIndex !== -1) {
                selfMessageCheck.metadata.lastMessages.splice(msgIndex, 1);
                logger.debug(`Found a duplicate from myself`);
                logger.debug(`${message.content}`);
                return;
            }
        }

        // TODO: Handle mentions, embeds
        const preparedMessage = discordMessageToIRC(ircClients,
            discordClient,
            client.metadata,
            logger,
            message);
        const channel = (message.channel as Discord.TextChannel).name;

        // Send the message off
        sendToIRCw(message.guild.id,
            `:${authorIrcName}!${message.author.id}@whatever PRIVMSG #${channel} :${preparedMessage}\r\n`,
            0);
    });
});

// Handler for when new channels are created
discordClient.on("channelCreate", (newChannel) => {
    // Ignore non-text channels
    if (newChannel.type !== "text") return;
    logger.debug("Caught a 'channelCreate");

    const channel = newChannel as Discord.TextChannel;
    const discordID = channel.guild.id;

    Object.values(ircClients).forEach((client) => {
        if (client.metadata.discordID !== discordID) return;

        // Create the new channel
        logger.debug(`Creaing new channel #${channel.name} (${channel.id})`);
        client.metadata.discordChannels = client.metadata.discordChannels.concat([{
            id: newChannel.id,
            discordName: channel.name,
            ircName: `#${channel.name}`,
            members: [],
        }]);
    });
});

// Handler for when a user changes his presence
discordClient.on("channelUpdate", (before, after) => {
    // Ignore non-text channels
    if (before.type !== "text") return;
    logger.debug("Caught a 'channelUpdate");

    const beforeCh = before as Discord.TextChannel;
    const beforeName = `#${beforeCh.name}`;
    const afterCh = after as Discord.TextChannel;
    const afterName = `#${afterCh.name}`;

    const discordID = beforeCh.guild.id;

    // Check if a name change was performed
    if (beforeCh.name !== afterCh.name) {
        Object.values(ircClients).forEach((client) => {
            // Don't bother with other servers
            if (client.metadata.discordID !== discordID) return;

            // Add the new channel
            client.metadata.discordChannels = client.metadata.discordChannels.concat([{
                id: afterCh.id,
                discordName: afterCh.name,
                ircName: afterName,
                members: [],
            }]);

            // Tell the connected clients that the name changed
            // and move them
            if (client.metadata.activeChannels.includes(beforeName)) {
                sendToIRCw(discordID,
                    `:discordIRCd!notReallyA@User PRIVMSG ${beforeName} :${beforeName} has been renamed to ${afterName} \r\n`,
                    0);

                const fParams: IFunctionParams = {
                    ircClients: ircClients,
                    logger: logger,
                    metadata: client.metadata,
                    discordClient: discordClient,
                    configuration,
                };

                partCommand(fParams, beforeName);
                joinCommand(fParams, afterName);

                const index = client.metadata.discordChannels.findIndex((channel) => {
                    return channel.ircName === beforeName;
                });
                if (index !== -1)
                    client.metadata.discordChannels.splice(index, 1);
            }
        });
    }

    // Did the topic change?
    if (beforeCh.topic !== afterCh.topic) {
        sendToIRCw(discordID,
            `:noboyknows!orCares@whatever TOPIC ${afterName} :${afterCh.topic}\r\n`,
            0);
    }
});

// Handler for when channels are deleted
discordClient.on("channelDelete", (oldChannel) => {
    // Ignore non-text channels
    if (oldChannel.type !== "text") return;
    logger.debug("Caught a 'channelDelete'");

    const channel = oldChannel as Discord.TextChannel;
    const ircChannel = `#${channel.name}`;

    sendToIRCw(channel.guild.id,
        `:discordIRCd!notReallyA@User PRIVMSG ${ircChannel} :${ircChannel} has been deleted \r\n`,
        0);

    Object.values(ircClients).forEach((client) => {
        // Is the client connected to the channel?
        if (!client.metadata.activeChannels.includes(ircChannel)) return;

        // Disconnect the clients from the channel
        partCommand({
            ircClients: ircClients,
            discordClient: discordClient,
            metadata: client.metadata,
            logger: logger,
            configuration: configuration,
        }, ircChannel);

        // Delete the channel from the discordChannels
        const index = client.metadata.discordChannels
            .findIndex((channel) => {
                return channel.ircName === ircChannel;
            });
        if (index !== -1)
            client.metadata.discordChannels.splice(index, 1);
    });
});

// TODO: This needs testing
discordClient.on("guildMemberUpdate", (before, after) => {
    const discordID = after.guild.id;

    const ircNickBefore = ircNickname(before.displayName,
        before.user.discriminator,
        before.user.bot);
    const ircNickAfter = ircNickname(after.displayName,
        after.user.discriminator,
        after.user.bot);

    logger.debug("Caught a 'guildMemberUpdate'");
    logger.debug(`Before: ${ircNickBefore}`);
    logger.debug(`After: ${ircNickAfter}`);

    // Did the name change?
    if (ircNickBefore !== ircNickAfter) {
        // Did *OUR* name change?
        if (after.id === discordClient.user.id) {
            sendToIRCw(discordID,
                `:${ircNickBefore}!${discordClient.user.id}@whatever NICK ${ircNickAfter}\r\n`,
                0);
        } else {
            // Find the client
            const client = Object.values(ircClients).find((c) => {
                return c.metadata.discordID === discordID;
            });
            if (!client) {
                logger.error(`Could not find the client connected to ${discordID}`)
                return;
            }

            // Check if the user is in one of our channels
            client.metadata.discordChannels.forEach((channel) => {
                // No one is in this channel
                if (channel.members.length === 0) return;

                // Is the user in the channel?
                const inChannel = channel.members.findIndex((member) => {
                    return member.ircNickname === ircNickBefore;
                });
                if (inChannel !== -1) {
                    // Remove the old user
                    channel.members.splice(inChannel, 1);

                    // Add the new user
                    channel.members = channel.members.concat([{
                        discordName: after.displayName,
                        discordState: after.presence.status,
                        ircNickname: ircNickAfter,
                        id: after.id,
                    }]);
                }
            });
            sendToIRCw(discordID,
                `:${ircNickBefore}!${after.user.id}@whatever NICK ${ircNickAfter}\r\n`,
                0);
        }
    } else {
        // Dunno
    }
});

// Check if the users in the channels on a server vary from the
// channels on the Discord Server they represent.
// If a user too much is found, then that user will be removed.
// If a user too few is found, then that user will be added
function guildMemberCheckChannels(discordID: string,
    guildMember: Discord.GuildMember) {
    Object.values(ircClients)
        .forEach((client) => {
            // Ignore clients that have a different Discord ID
            const { metadata } = client;
            if (metadata.discordID !== discordID) return;

            metadata.discordChannels.forEach((channel) => {
                // Only do stuff for the active ones
                // TODO: If we have multiple clients for the same server
                //       this could lead to excessive API-Calls. Restrict
                //       the access to the server so, that only one client
                //       can be connected to one Discord Server at a time.
                if (!metadata.activeChannels.includes(channel.ircName)) return;

                const { displayName, user, presence, id } = guildMember;
                const { discriminator, bot } = user;
                const ircNick = ircNickname(displayName, discriminator, bot);

                // Fetch the users on that particular channel
                // NOTE: We can safely assume that the channel is a text channel
                //       as we only add those to metadata.discordChannels. So an
                //       explicit type check is not neccessary.
                const textChannel = discordClient.guilds.get(discordID)
                    .channels.get(channel.id) as Discord.TextChannel;
                const members = textChannel.members.array();

                // Find the guildMember
                const inDiscord = Boolean(members.find((dm) => {
                    return displayName === dm.displayName &&
                        (presence.status !== "offline" || configuration.showOffline);
                }));
                const inIRC = Boolean(channel.members
                    .map((member) => member.ircNickname)
                    .find((memberIRCNick) => memberIRCNick === ircNick));

                // If the user is on the Discord Server, but not on the IRC,
                // we'll need to add him
                if (!inIRC && inDiscord) {
                    channel.members = channel.members.concat([{
                        discordName: displayName,
                        discordState: presence.status,
                        ircNickname: ircNick,
                        id: id,
                    }]);

                    // Tell the client about it
                    sendToIRCw(discordID,
                        `:${ircNick}!${guildMember.id}@whatever JOIN ${channel.ircName}\r\n`,
                        metadata.ircID);

                    // Should we notify the client about the new user?
                    if (metadata.awayNotify) {
                        const msg = {
                            idle: "Idle",
                            offline: "Offline",
                            dnd: "Do not disturb",
                        };

                        // TODO: Honor configuration.showOffline
                        sendToIRCw(discordID,
                            `:${ircNick}!${id}@whatever AWAY:${msg[presence.status]}\r\n`,
                            metadata.ircID);
                    }
                }

                // If the user is on the IRC server but not on the Discord server,
                // we'll need to remove him and tell the clients
                if (inIRC && !inDiscord) {
                    // Find the user and remove him
                    // TODO: Check if findIndex does *not* return -1
                    const index = channel.members.findIndex((member) => {
                        return member.id === guildMember.id;
                    });
                    channel.members.splice(index, 1);

                    // Tell the client
                    sendToIRCw(discordID,
                        `:${ircNick}!${id}@whatever PART ${channel.ircName}\r\n`,
                        metadata.ircID);
                }
            });
        });
}

// Sends a message to the client that the guildMember is no longer
// on the server.
function guildMemberNoMore(discordID: string,
    noMoreReason: string,
    guildMember: Discord.GuildMember) {
    // Find the client connected to the Discord server
    const client = Object.values(ircClients)
        .find((client) => client.metadata.discordID === discordID);

    if (!client) return;

    // Is the user connected to any channels that the client is
    // active in? If so, remove him.
    // If the user did not leave because he went offline, we need
    // to send a message to the client
    const userChannels = client.metadata.discordChannels.filter((channel) => {
        // We're only interested in active channels
        return client.metadata.activeChannels.includes(channel.ircName);
    }).filter((channel) => {
        // We're only interested in channels the guildMember was in
        return channel.members.find((member) => member.id === guildMember.id);
    });

    const { displayName, user, presence, id } = guildMember;
    const { discriminator, bot } = user;
    const ircNick = ircNickname(displayName, discriminator, bot);

    if (userChannels.length !== 0) {
        // Send the client a goodbye message
        sendToIRCw(discordID,
            `:${ircNick}!${id}@whatever QUIT :${noMoreReason}\r\n`,
            client.metadata.ircID);
    }

    // Remove the user from the channels
    userChannels.forEach((channel) => {
        // TODO: index !== -1
        const index = channel.members.findIndex((member) => {
            return member.id === guildMember.id
        });
        channel.members.splice(index, 1);
    });
}

discordClient.on("presenceUpdate", (before, after) => {
    // We don't need to send a message to specific clients,
    // so we just check if any are connected
    if (Object.keys(ircClients).length === 0) return;

    const discordID = after.guild.id;
    const client = Object.values(ircClients).find((client) => {
        return client.metadata.discordID === discordID;
    });

    // Prevent us from processing updates from servers, we do not
    // care about
    if (!client) return;
    logger.debug("Caught a 'presenceUpdate'");

    const { bot, discriminator } = after.user;
    const ircNick = ircNickname(after.displayName, discriminator, bot);

    const oldPres = before.presence.status;
    const newPres = after.presence.status;

    if (oldPres === "offline" && !configuration.showOffline) {
        // Ensure that the user appears inside the channels
        guildMemberCheckChannels(discordID, after);
    } else if (newPres === "offline" && !configuration.showOffline) {
        // Offline notice
        guildMemberNoMore(discordID, "User gone offline", after);
    } else if (configuration.showOffline) {
        Object.values(ircClients).forEach((client) => {
            if (!client.metadata.awayNotify) return;

            if (newPres === "offline") {
                sendToIRCw(discordID,
                    `:${ircNick}!${after.id}@whatever AWAY :Offline\r\n`,
                    client.metadata.ircID);
            } else if (newPres === "dnd") {
                sendToIRCw(discordID,
                    `:${ircNick}!${after.id}@whatever AWAY :Do not disturb\r\n`,
                    client.metadata.ircID);
            } else if (newPres === "idle") {
                sendToIRCw(discordID,
                    `:${ircNick}!${after.id}@whatever AWAY :Idle\r\n`,
                    client.metadata.ircID);
            } else if (oldPres !== "offline" && newPres === "online") {
                sendToIRCw(discordID,
                    `:${ircNick}!${after.id}@whatever AWAY\r\n`,
                    client.metadata.ircID);
            }
        });
    }
});

// Send a message to all clients, regardless of their
// Discord- or IRC-ID
function sendGeneralNotice(message: string) {
    Object.values(ircClients).forEach((client) => {
        const { discordID, ircID, iDisplayName } = client.metadata;
        sendToIRCw(discordID,
            `:${configuration.irc.hostname} NOTICE ${iDisplayName} :${message}\r\n`,
            ircID);
    });
}

function clientsInChannel(ircChannel: string): IIRCClient[] {
    const clients = Object.values(ircClients).filter((client) => {
        return client.metadata.activeChannels.includes(ircChannel);
    });

    return clients;
}

const server = net.createServer(netOpts, (socket: MarkedSocket) => {
    logger.info("Creating server");
    socket.setEncoding("utf8");

    // Mark the socket
    const uuid = genUUID();

    socket.uuid = uuid;
    ircClients[uuid] = {
        socket: socket,
        metadata: {
            isCAPBlocked: false,
            nickname: "",
            username: "",
            awayNotify: false,
            connectArray: [],
            authenticated: false,
            pongCount: 1,

            ircID: Object.keys(ircClients).length,
            uuid: uuid,
            discordChannels: [],
            activeChannels: [],
            lastMessages: [],

            discordID: "",
            dDisplayName: "",
            iDisplayName: "",
        }
    };

    // Just do nothing, when we hit an error
    socket.on("error", (err) => {
        logger.error("Socket error!");
        logger.error(err);
    });

    socket.on("data", (data: string) => {
        // For debugging
        if (configuration.debug) {
            // Remove a linebreak, if data ends with \r?\n
            // NOTE: TypeScript does not believe that data is a string
            //@ts-ignore
            const lines = data.split(/\r?\n/);
            const dataStr = lines.length === 1 ? data : lines[0];

            logger.debug(`Received data: ${dataStr}`);
        }

        // Data can be multiline
        //@ts-ignore
        let dataArray = data.match(/.+/g);
        dataArray.forEach((line) => {
            const { command, sender, params } = parseMessage(line);
            const { metadata } = clientFromUUID(ircClients, socket.uuid);
            logger.debug(`Sender: ${sender}; Command: ${command}; Params: ${params}`);

            const functionParams = {
                ircClients: ircClients,
                metadata: metadata,
                discordClient: discordClient,
                configuration: configuration,
                logger: logger,
            };

            if (command == "CAP") {
                const subcmd = params[0];
                if (!metadata.nickname)
                    metadata.nickname = "*";

                switch (subcmd) {
                    case "LS":
                        metadata.isCAPBlocked = true;
                        socket.write(
                            `:${configuration.irc.hostname} CAP ${metadata.nickname} LS :away-notify\r\n`
                        );
                        break;
                    case "LIST":
                        if (metadata.awayNotify)
                            socket.write(
                                `:${configuration.irc.hostname} CAP ${metadata.nickname} LIST :away-notify\r\n`
                            );
                        else
                            socket.write(
                                `:${configuration.irc.hostname} CAP ${metadata.nickname} LIST :\r\n`
                            );
                        break;
                    case "REQ":
                        const req_caps = params[1];

                        if (req_caps == "away-notify") {
                            socket.write(
                                `:${configuration.irc.hostname} CAP ${metadata.nickname} ACK :away-notify\r\n`
                            );
                            metadata.awayNotify = true;
                        } else if (req_caps == "-away-notify") {
                            socket.write(
                                `:${configuration.irc.hostname} CAP ${metadata.nickname} ACK :away-notify\r\n`
                            );
                            metadata.awayNotify = false;
                        } else {
                            socket.write(
                                `:${configuration.irc.hostname} CAP ${metadata.nickname} NAK :${req_caps}\r\n`
                            );
                        }

                        socket.write(
                            `:${configuration.irc.hostname} CAP ${metadata.nickname} ACK :${params[0]}\r\n`
                        );
                        break;
                    case "ACK":
                        // Could be useful, but we'll ignore it
                        break;
                    case "END":
                        metadata.isCAPBlocked = false;

                        // Check if we're authenticated and if all the welcome things have been sent.
                        if (metadata.connectArray.length > 0) {
                            metadata.connectArray.forEach((line) => socket.write(line));

                            metadata.connectArray = [];
                        }

                        break;
                    default:
                        // WEEE WOOO
                        socket.write(`:${configuration.irc.hostname} 410 * ${subcmd} :Invalid CAP command\r\n`);
                        break;
                }
            } else if (!metadata.authenticated) {
                switch (command) {
                    case "PASS":
                        // Check if a client is not already connected to this server
                        const found = Boolean(Object.values(ircClients)
                            .find((client) => client.metadata.discordID === params[0]));

                        if (found) {
                            socket.write(
                                `:${configuration.irc.hostname} 464 :A client is alreay connected to this Discord server\r\n`
                            );

                            // Close the connection
                            socket.end();
                            return;
                        }

                        // We use the password as the ID of the discord server
                        // that we'll connect to
                        metadata.discordID = params[0];
                        break;
                    case "NICK":
                        // Set the nickname
                        metadata.nickname = params[0];
                        break;
                    case "USER":
                        userCommand(functionParams, params[0], params[1], socket);
                        break;
                }
            }

            if (metadata.authenticated && !metadata.isCAPBlocked) {
                switch (command) {
                    case "LIST":
                        listCommand(functionParams);
                        break;
                    case "PING":
                        pingCommand(functionParams);
                        break;
                    case "QUIT":
                        quitCommand(functionParams);
                        break;
                    case "PART":
                        // The user can leave multiple channels at the same
                        // time
                        params[0].split(",")
                            .forEach((channel) => {
                                partCommand(functionParams, channel);
                            });

                        break;
                    case "JOIN":
                        // The user can join multiple channels at the same time
                        params[0].split(",")
                            .forEach((channel) => {
                                joinCommand(functionParams, channel);
                            });
                        break;
                    case "PRIVMSG":
                        privmsgCommand(functionParams, params[0], params[1]);
                        break;
                }
            }
        });

    });

    socket.on("end", () => {
        socket.end();
        delete ircClients[socket.uuid];
    });
});

// Done. Start listening
server.listen(6667);
