import { IFunctionParams } from "./types";
import { sendToIRC } from "../irc";

// Answer a ping from the client
export function pingCommand(params: IFunctionParams) {
    const { ircClients, metadata, logger, configuration } = params;
    sendToIRC(metadata.discordID,
        logger,
        ircClients,
        `:${configuration.irc.hostname} PONG ${configuration.irc.hostname} :${metadata.pongCount++}\r\n`,
        metadata.ircID);

}
