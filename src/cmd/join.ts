import { TextChannel } from "discord.js";

import { joinMax } from "../string";

import { IFunctionParams } from "./types";
import { fetchDiscordChannels, discordMessageToIRC } from "../discord";
import {
    sendToIRC, isClientInChannel, channelNameToID,
    ircNickname, IRC_MAX_MESSAGE_LENGTH, channelNameToChannel,
    sendMessageToIRC
} from "../irc";

// Join a channel
export function joinCommand(params: IFunctionParams,
    ircChannel: string) {
    // Unpack values for convenience
    const { metadata, discordClient, logger, configuration,
        ircClients } = params;

    params.logger.debug(
        `User ${metadata.username} attempts to join ${ircChannel}`
    );

    // Check if we're not already inside that channel
    if (isClientInChannel(metadata, ircChannel)) return;

    // Initial fetching of the channel list
    // TODO: Move somewhere else? discordClient.on("ready", ...) perhaps.
    logger.info(`Initially fetching channel list for ${metadata.discordID}`);
    fetchDiscordChannels(discordClient, metadata);

    // Check if the channel exists
    const discordChannelNames = metadata.discordChannels.map((channel) => {
        return channel.ircName;
    });
    if (!discordChannelNames.includes(ircChannel)) {
        logger.debug(`Attempt to enter non-existant channel ${ircChannel}`);
        // TOFIX: Duplicate channels appear in the list
        logger.debug(`Known channels: ${discordChannelNames}`);

        return;
    }

    const channelID = channelNameToID(metadata, ircChannel);

    logger.debug(`Found channel ${ircChannel} (${channelID})`);

    const { iDisplayName, uuid } = metadata;

    const channelContent = discordClient.channels.get(channelID);
    const { topic } = channelContent as TextChannel;

    const members = (channelContent as TextChannel).members.array()
        .map((member) => {
            const { displayName, user } = member;
            const { bot, discriminator } = user;
            const memberNick = ircNickname(displayName, discriminator, bot);

            return {
                discordName: displayName,
                discordState: member.presence.status,
                ircNickname: memberNick,
                id: member.id,
            };
        });

    // Set channel members
    ircClients[metadata.uuid].metadata.discordChannels.find((channel) => {
        return channel.id === channelID;
    }).members = members;

    // Send all the IRC nicknames space-separated, but split them up if the line
    // exceeds IRC's maximum message length
    const memberStrings = joinMax(members.map((member) => member.ircNickname),
        IRC_MAX_MESSAGE_LENGTH);

    logger.debug(`Members: ${members}`);
    logger.debug(`memberStrings: ${memberStrings}`);

    logger.debug(`:${iDisplayName} JOIN ${ircChannel}\r\n`);
    // Send the message that we joined the channel
    sendToIRC(metadata.discordID,
        logger,
        params.ircClients,
        `:${iDisplayName} JOIN ${ircChannel}\r\n`,
        metadata.ircID);

    // Send the topic of the channel
    sendToIRC(metadata.discordID,
        logger,
        params.ircClients,
        `:${configuration.irc.hostname} 332 ${iDisplayName} ${ircChannel} :${topic}\r\n`,
        metadata.ircID);

    const nowSeconds = (new Date()).getTime() / 1000;
    sendToIRC(metadata.discordID,
        logger,
        params.ircClients,
        `:${configuration.irc.hostname} 333 ${iDisplayName} ${ircChannel} noboyknows!orCares@whatever ${nowSeconds}\r\n`,
        metadata.ircID);

    // Tell the client about the members
    memberStrings.forEach((line) => {
        sendToIRC(metadata.discordID,
            logger,
            params.ircClients,
            `:${configuration.irc.hostname} 353 ${iDisplayName} @ ${ircChannel} :${line}\r\n`,
            metadata.ircID);
    });

    // Tell the client that we sent all members
    sendToIRC(metadata.discordID,
        logger,
        params.ircClients,
        `:${configuration.irc.hostname} 366 ${iDisplayName} ${ircChannel} :End of /NAMES list.\r\n`,
        metadata.ircID);

    if (metadata.awayNotify) {
        const channel = channelNameToChannel(metadata, ircChannel);

        channel.members.forEach((member) => {
            if (member.discordState === "offline" && !configuration.showOffline)
                return;

            const msg = {
                idle: "Idle",
                dnd: "Do not disturb",
                offline: "Offline",
            };

            sendToIRC(metadata.discordID,
                logger,
                params.ircClients,
                `:${iDisplayName}!${member.id}@whatever AWAY :${msg[member.discordState]}\r\n`,
                metadata.ircID);
        });
    }

    // Add this channel to the watchlist
    metadata.activeChannels = metadata.activeChannels.concat([ircChannel]);

    logger.debug(Object.values(ircClients));

    // Send the last n messages
    (channelContent as TextChannel).fetchMessages({
        limit: configuration.discord.historyFetchLimit
    }).then((messages) => {
        messages.sort((a, b) => {
            //@ts-ignore
            return a.createdAt - b.createdAt;
        }).forEach((msg) => {
            // We need to preprocess the message to replace mentions with
            // their IRC counterparts
            const msgText = discordMessageToIRC(ircClients,
                discordClient,
                metadata,
                logger,
                msg);
            // We check if the message we're about to send has more than 1 line.
            // If it does, then we need to send them one by one. Otherwise the client
            // will try to interpret them as commands.
            const lines = msgText.split(/\r?\n/);
            const authorName = msg.member ?
                msg.member.displayName : msg.author.username;
            const { bot, discriminator } = msg.author;
            const authorIrcName = ircNickname(authorName, discriminator, bot);

            if (lines.length > 1) {
                for (let i = 0; i < lines.length; i++)
                    sendMessageToIRC(ircChannel,
                        ircClients,
                        logger,
                        metadata.discordID,
                        authorIrcName,
                        msg.author.id,
                        lines[i]
                    );
            } else {
                sendMessageToIRC(ircChannel,
                    ircClients,
                    logger,
                    metadata.discordID,
                    authorIrcName,
                    msg.author.id,
                    msgText);
            }
        });
    });
}
