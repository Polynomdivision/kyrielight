import { TextChannel } from "discord.js";

import { joinMax } from "../string";

import { IFunctionParams } from "./types";
import { fetchDiscordChannels } from "../discord";
import {
    sendToIRC, isClientInChannel, channelNameToID,
    ircNickname, IRC_MAX_MESSAGE_LENGTH, channelNameToChannel,
    sendMessageToIRC, MarkedSocket
} from "../irc";

// Join a channel
export function userCommand(params: IFunctionParams,
    username: string, altUsername: string, socket: MarkedSocket) {
    const { discordClient, metadata, logger, configuration } = params;



    // Check if the user that wants to connect matches
    // the user in the config file
    if (![username, altUsername].includes(configuration.irc.username)) {
        // Not who we expect; Bail out
        logger.error(`${metadata.nickname}: Failed to connect to ${metadata.discordID}`);
        socket.write(
            `:${configuration.irc.hostname} 464 ${metadata.nickname} :Failed to connect to ${metadata.discordID}\r\n`
        );
        socket.end();
        return;
    }

    // Set more metadata
    metadata.username = params[0];

    if (metadata.discordID === "DMserver") {
        // Rename the IRC user to his discord self
        const { username, discriminator } = discordClient.user;
        const newNick = ircNickname(username, discriminator);

        metadata.dDisplayName = username;
        metadata.iDisplayName = newNick;
        metadata.authenticated = true;

        const connectArray = [
            `:${metadata.nickname}!${discordClient.user.id}@whatever NICK ${newNick}\r\n`,
            `:${configuration.irc.hostname} 001 ${newNick} :Welcome to the fake Internet Relay Chat Network ${newNick}\r\n`,
            `:${configuration.irc.hostname} 003 ${newNick} :This server was created specifically for you\r\n`
        ];
        connectArray.forEach((msg) => socket.write(msg));
    } else if (discordClient.guilds.get(metadata.discordID)) {
        // Fetch the name from the Discord API
        const clientID = discordClient.user.id;
        discordClient.guilds.get(metadata.discordID)
            .fetchMember(clientID).then((gMember) => {
                const { displayName, user } = gMember;
                const { discriminator } = user;
                const newNick = ircNickname(displayName, discriminator);

                metadata.dDisplayName = displayName;
                metadata.iDisplayName = newNick;
                metadata.authenticated = true;

                const connectArray = [
                    `:${metadata.nickname}!${discordClient.user.id}@whatever NICK ${newNick}\r\n`,
                    `:${configuration.irc.hostname} 001 ${newNick} :Welcome to the fake Internet Relay Chat Network ${newNick}\r\n`,
                    `:${configuration.irc.hostname} 003 ${newNick} :This server was created specifically for you\r\n`
                ];

                // If we are waiting for CAP negotiation,
                // store the connect array for later
                if (metadata.isCAPBlocked)
                    metadata.connectArray = connectArray;
                else
                    connectArray.forEach((msg) =>
                        socket.write(msg)
                    );
            });
    } else {
        // We cannot figure out the Discord display name; bail out
        socket.write(`:${configuration.irc.hostname} 464 ${metadata.nickname} :Invalid login.  Make sure your username matches the username set in the config.\r\n`);
        socket.end();
    }

}
