import { TextChannel } from "discord.js";

import { IFunctionParams } from "./types";
import {
    sendToIRC, sendBotMessage, ircNickname, channelNameToID,
    isClientInChannel
} from "../irc";

// Leave a channel
export function partCommand(params: IFunctionParams,
    ircChannel: string) {
    const { metadata, logger, ircClients, discordClient } = params;

    logger.debug(`Parting from channel ${ircChannel}`);

    // Check if the channel is valid, i.e. the user is inside the channel
    if (!isClientInChannel(metadata, ircChannel)) return;

    // Remove the channel
    const activeIndex = metadata.activeChannels.findIndex((channel) => {
        return channel === ircChannel;
    });
    const discordIndex = metadata.discordChannels.findIndex((channel) => {
        return channel.ircName === ircChannel;
    });
    metadata.activeChannels.splice(activeIndex, 1);
    metadata.discordChannels[discordIndex].members = [];

    // Broadcast that we just left the channel
    const { iDisplayName } = metadata;
    sendToIRC(metadata.discordID,
        logger,
        ircClients,
        `:${iDisplayName}!${discordClient.user.id}@whatever PART ${ircChannel}\r\n`,
        metadata.ircID);
}
