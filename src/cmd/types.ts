import { Client } from "discord.js";

import { Logger } from "winston";

import { IClientMetadata, IIRCClient } from "../irc";

export type Config = {
    // Show debug output
    debug: boolean;
    // Show offline users
    showOffline: boolean;
    // Show notifications,
    notify: boolean;
    // Discord specific options
    discord: {
        // How many messages to fetch at max when joining a channel
        historyFetchLimit: number;
        // Where to get the token from
        tokenStore: string;
        // The Discord API token
        token: string;
    };
    // IRC specific options
    irc: {
        // The port to listen on
        port: number;
        // Hostname
        hostname: string;
        // The only username that will be accepted during authentication
        username: string;
        // TLS specific options
        tls?: {
            // Use TLS
            enabled: boolean;
            // Location of the certificate and the private key
            key: string;
            cert: string;
        };
    };
};

export type ClientMapping = { [key: string]: IIRCClient };

export interface IFunctionParams {
    ircClients: ClientMapping;
    metadata: IClientMetadata;
    discordClient: Client;
    configuration: Config;
    logger: Logger;
};
