import { TextChannel } from "discord.js";

import { joinMax } from "../string";

import { IFunctionParams } from "./types";
import { fetchDiscordChannels } from "../discord";
import {
    sendToIRC, isClientInChannel, channelNameToID,
    ircNickname, IRC_MAX_MESSAGE_LENGTH, channelNameToChannel,
    sendMessageToIRC, clientFromIrcID
} from "../irc";

export function listCommand(params: IFunctionParams) {
    const { logger, discordClient, ircClients, metadata,
        configuration } = params;
    const { discordID, ircID } = metadata;

    // Exclude the DMserver
    logger.debug(`List command for discordID "${discordID}"`);
    if (discordID === "DMserver") return;

    const client = clientFromIrcID(ircClients, metadata.ircID);
    const { nickname } = metadata;
    const { socket } = client;
    const channels = discordClient.guilds.get(discordID).channels.array();

    sendToIRC(discordID,
        logger,
        ircClients,
        `:${configuration.irc.hostname} 321 ${nickname} Channel :Users Name\r\n`,
        ircID);
    channels.forEach((channel) => {
        // Exclude non-text channels
        if (channel.type !== "text") return;

        // Send data about the channels
        const { name, members, topic } = (channel as TextChannel);
        const filteredTopic = topic ? topic.replace(/\r?\n/gi, " ") : "[No Topic]";
        const memberCount = members.array().length;
        sendToIRC(discordID,
            logger,
            ircClients,
            `:${configuration.irc.hostname} 322 ${nickname} #${name} ${memberCount} :${filteredTopic}\r\n`,
            ircID);
    });
    sendToIRC(discordID,
        logger,
        ircClients,
        `:${configuration.irc.hostname} 323 ${nickname} :End of channel list.\r\n`,
        ircID);
}
