import { TextChannel, Role, PresenceData } from "discord.js";

import { IFunctionParams } from "./types";
import {
    IChannelMember, sendToIRC, sendBotMessage, ircNickname, channelNameToID
} from "../irc";

export function privmsgCommand(params: IFunctionParams,
    recipient: string,
    message: string) {
    const { metadata, ircClients, discordClient, logger } = params;

    const discordUserFromIrc = (name: string) => {
        const findFunc = (user) => {
            const nick = ircNickname(user.username, user.bot, user.disciminator);

            return nick === name;

        };
        if (metadata.discordID === "DMserver") {
            return discordClient.users.array().find(findFunc);
        } else {
            return discordClient.guilds.get(metadata.discordID)
                .members.array().find(findFunc);
        }
    };

    // Debug.
    // Just so we have an insight into the server
    if (message.startsWith("!mashu")) {
        // Strip away the "!mashu" at the beginning
        //const cmd_raw = message.substr("!mashu".length + 1, message.length);
        const cmd_raw = message.split(" ");
        const cmd = cmd_raw[1];

        switch (cmd) {
            case "active_channels":
                sendBotMessage(metadata,
                    ircClients,
                    logger,
                    `${metadata.activeChannels.join(",")}`);
                break;
            case "test":
                sendBotMessage(metadata, ircClients, logger, "Senpai~~");
                break;
            case "friends":
                discordClient.user.friends.array().forEach(friend => {
                    sendBotMessage(metadata,
                        ircClients,
                        logger,
                        `${friend.username} [${friend.presence.status}]`);
                });
                break;
            case "servers":
                discordClient.guilds.array().forEach(guild => {
                    sendBotMessage(metadata,
                        ircClients,
                        logger,
                        `${guild.name}: ${guild.id}`);
                });
                break;
            case "status":
                if (cmd_raw.length < 3) {
                    sendBotMessage(metadata,
                        ircClients,
                        logger,
                        `Sorry. You did not send enough arguments`);
                    return;
                }

                const presence = cmd_raw[2];
                if (["online", "idle", "invisible", "dnd"].indexOf(presence) === -1) {
                    sendBotMessage(metadata,
                        ircClients,
                        logger,
                        `Sorry. I can't set your presence to that!`);
                    return;
                }

                discordClient.user.setPresence({
                    status: presence,
                } as PresenceData);

                sendBotMessage(metadata,
                    ircClients,
                    logger,
                    `Got it!`);
                break;
            case "msg":
                if (cmd_raw.length < 3) {
                    sendBotMessage(metadata,
                        ircClients,
                        logger,
                        `Sorry. You did not send enough arguments`);
                    return;
                }

                // Find the user in the clients friend list
                const recipient = cmd_raw[2];
                const user = discordClient.user.friends.array().find(u => {
                    const { bot, discriminator, username } = u;
                    const authorIrcName = ircNickname(username, discriminator, bot);

                    return authorIrcName === recipient;
                });

                if (!user) {
                    sendBotMessage(metadata,
                        ircClients,
                        logger,
                        `Sorry. I don't know who you mean...`);
                    sendBotMessage(metadata,
                        ircClients,
                        logger,
                        `I can't find ${recipient}`);
                    return;
                }

                // Concat everything that follows the recipient
                const msg = cmd_raw.slice(3).concat().reduce((acc, curr) => {
                    return acc + " " + curr;
                });
                user.send(msg);
                break;
        }

        return;
    }

    if (recipient.startsWith("#")) {
        const channelID = channelNameToID(metadata, recipient);
        const channel = discordClient.channels.get(channelID) as TextChannel;
        // Process the message some more
        // Find user mentions starting with a "@"
        const user_pattern = /\@[^\&]\S+/gi;
        const user_pat_matches = message.match(user_pattern);
        let processedMessage = message;

        if (user_pat_matches) {
            logger.debug(`Found user matches: ${user_pat_matches}`);
            user_pat_matches.forEach(match => {
                // Look the user up
                // Remove the leading '@'
                const user_mention = match.slice(1);
                const { members } = (params.metadata.discordChannels
                    .find(ch => ch.discordName === channel.name) || { members: [] });

                // Find the member we're probably trying to ping
                const member = (members as IChannelMember[]).find((m: IChannelMember) => {
                    return m.ircNickname === user_mention
                });

                // Replace the mention with a Discord specific string if we were
                // able to figure out who we're trying to ping. Otherwise, let
                // Mashu tell the client that we have no clue.
                if (member) {
                    processedMessage = processedMessage.replace(match, `<@${member.id}>`);
                } else {
                    sendBotMessage(metadata,
                        ircClients,
                        logger,
                        `Sorry! I couldn't find out who you're trying to mention...`);
                    sendBotMessage(metadata,
                        ircClients,
                        logger,
                        `I don't know "${user_mention}"`);
                }
            });
        }

        const role_pattern = /\@\&\S+/gi;
        const role_pat_matches = processedMessage.match(role_pattern);
        if (role_pat_matches) {
            logger.debug(`Gound role matches: ${role_pat_matches}`);
            role_pat_matches.forEach(match => {
                // First extract the role that we're trying to mention
                const role_mention = match.slice(2);

                const roles = discordClient.guilds.get(metadata.discordID).roles;
                if (!roles)
                    return;

                const role = roles.array().find(r => {
                    return r.name === role_mention;
                });

                if (role) {
                    processedMessage = processedMessage.replace(match, `<@&${role.id}>`);
                } else {
                    // Transform it into a "discord-like" format
                    processedMessage = processedMessage.replace(match, `@${role_mention}`);
                    sendBotMessage(metadata,
                        ircClients,
                        logger,
                        `Sorry! I couldn't find out which group you're trying to mention...`);
                    sendBotMessage(metadata,
                        ircClients,
                        logger,
                        `I don't know "${role_mention}"`);
                }
            });
        }

        // Store the sent message, so that we can ignore it in the
        // on-message handler
        metadata.lastMessages.push(processedMessage);
        channel.send(processedMessage);
    } else if (recipient !== "discordIRCd") {
        const recipUser = discordUserFromIrc(recipient);
        recipUser.sendMessage(message);

        if (metadata.discordID !== "DMserver") {
            sendBotMessage(metadata,
                ircClients,
                logger,
                `PM Send: Note that replies will not arrive here but on the PM server`
            );
        }
    }

}
