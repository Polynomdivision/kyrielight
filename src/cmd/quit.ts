import { IFunctionParams } from "./types";
import { sendToIRC, clientFromUUID } from "../irc";

// Answer a ping from the client
export function quitCommand(params: IFunctionParams) {
    const { ircClients, logger, metadata } = params;

    // Close the socket and remove the client from the known list
    const { uuid } = metadata;
    const client = clientFromUUID(ircClients, uuid);
    client.socket.end();

    // Remove the client from the list of clients we keep track of
    delete ircClients[uuid];
    logger.debug(`Killing user ${metadata.nickname}`);
    logger.debug(`CLIENTS: ${ircClients}`);

}
