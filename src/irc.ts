import { Socket } from "net";

import { Logger } from "winston";

import { ClientMapping } from "./cmd/types";

type MarkedSocket = Socket & { uuid: number; };
interface IIRCCommand {
    sender: string;
    command: string;
    params: string[];
};
export interface IChannelMember {
    discordName: string;
    discordState: string;
    ircNickname: string;
    id: string;
}
export interface IChannel {
    id: string;
    discordName: string;
    ircName: string;
    members: IChannelMember[];
};
interface IClientMetadata {
    isCAPBlocked: boolean;
    nickname: string;
    username: string;
    awayNotify: boolean;
    connectArray: string[];
    authenticated: boolean;
    pongCount: number;

    ircID: number;
    uuid: number;
    // The channels Discord tells us we have
    discordChannels: IChannel[];
    // The channels that the client is active in
    activeChannels: string[];
    // The last sent messages (So we can ignore them in the message handler)
    lastMessages: string[];

    discordID: string;
    // Discord display name
    dDisplayName: string;
    // IRC display name
    iDisplayName: string;
};
interface IIRCClient {
    socket: MarkedSocket;
    metadata: IClientMetadata;
}

export const IRC_MAX_MESSAGE_LENGTH = 510;

// Function that parses irc messages. 
// Shamelessly stolen from node-irc https://github.com/aredridel/node-ircd
export function parseMessage(line) {
    let message: IIRCCommand = {
        params: [],
        sender: "",
        command: ""
    };
    let m = /(:[^ ]+ )?([A-Z0-9]+)(?: (.*))?/i.exec(line);
    if (!m) {
        message['error'] = 'Unable to parse message';
    } else {
        let i;
        if (m[3] && (i = m[3].indexOf(':')) !== -1) {
            let rest = m[3].slice(i + 1);
            message.params = m[3].slice(0, i - 1).split(' ');
            message.params.push(rest);
        } else {
            if (m[3]) {
                message.params = m[3].split(' ');
            } else {
                message.params = [];
            }
        }
        if (m[2]) {
            message.command = m[2].toUpperCase();
        }
        if (m[1]) {
            message.sender = m[1];
        }
    }
    return message;
}

// Sends a message to the user with ircID @ircId
// If @ircID is 0, then that means that the message will be sent to
// every client that is connected to @discordID, authenticated and
// not CAP-blocked
export function sendToIRC(discordID: string,
    logger: Logger,
    ircClients: ClientMapping,
    line: string,
    ircID: number = 0) {
    const client = Object.values(ircClients).find((el) => {
        const { metadata } = el;

        return metadata.discordID === discordID &&
            metadata.authenticated &&
            !metadata.isCAPBlocked &&
            (ircID === 0 || ircID === ircID);
    });

    if (client) {
        // Debug only
        const lines = line.split(/\r?\n/);
        const lineStr = lines.length === 1 ? line : lines[0];

        logger.debug(`Sending: ${lineStr}`);
        client.socket.write(line);
    } else {
        logger.warn(`Client with ircID "${ircID}" not found!`);
    }
}

// Checks if a client is connected to a certain channel
export function isClientInChannel(metadata: IClientMetadata,
    ircChannel: string): boolean {
    return metadata.activeChannels.includes(ircChannel);
}

export function channelNameToChannel(metadata: IClientMetadata,
    ircChannel: string): IChannel {
    return metadata.discordChannels.find((channel) => {
        return channel.ircName === ircChannel;
    });
}

export function sendMessageToIRC(ircChannel: string,
    clients: ClientMapping,
    logger: Logger,
    discordID: string,
    ircNickname: string,
    authorID: string,
    message: string) {
    sendToIRC(discordID,
        logger,
        clients,
        `:${ircNickname}!${authorID}@whatever PRIVMSG ${ircChannel} :${message}\r\n`,
        0
    );
}

// Send a message to the client as everyone's favourite in
// bot-form: Mashu!
export function sendBotMessage(metadata: IClientMetadata,
    clients: ClientMapping,
    logger: Logger,
    message: string) {
    metadata.discordChannels.filter((channel) => {
        return metadata.activeChannels.includes(channel.ircName);
    }).forEach((channel) => {
        sendMessageToIRC(channel.ircName,
            clients,
            logger,
            metadata.discordID,
            "Mashu[BOT]",
            "definetelyNotA@User",
            `>>> ${message}`);
    });
}

// Maps an IRC channel name to its Discord ID
export function channelNameToID(metadata: IClientMetadata,
    channelName: string): string | undefined {
    const channel = metadata.discordChannels.find((channel) => {
        return channel.ircName === channelName;
    });

    return channel ? channel.id : undefined;
}

// Converts a display name that discord accepts into one
// that is IRC-suitable
export function ircNickname(discordDisplayName: string,
    discriminator: string,
    isBot: boolean = false): string {
    const replaceRegex = /[^a-zA-Z0-9_\\[\]\{\}\^`\|]/g;
    const shortenRegex = /_{1,}/g;

    // Sanitize the display name, if we need to
    if (replaceRegex.test(discordDisplayName)) {
        let sanitizedName = discordDisplayName.replace(replaceRegex, "_")
            .replace(shortenRegex, "_") + `${discriminator}`;
        return isBot ? sanitizedName + "[BOT]" : sanitizedName;
    } else {
        return isBot ? discordDisplayName + "[BOT]" : discordDisplayName;
    }
}

// Return the client from @ircClient that has a matching UUID
export function clientFromUUID(ircClients: ClientMapping,
    uuid: number): IIRCClient {
    return ircClients[uuid];
}

export function clientFromIrcID(ircClients: ClientMapping,
    ircID: number) {
    return Object.values(ircClients).find((client) => {
        return client.metadata.ircID === ircID;
    });
}

// Turns a Discord message into an IRC friendly format
export function discordToIRC(line: string): string {
    return "";
}

export { MarkedSocket, IIRCCommand, IClientMetadata, IIRCClient }
