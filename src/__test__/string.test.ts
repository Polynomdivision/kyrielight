import { joinMax } from "../string";

describe("conatMax should only return multiple lines when maxLength is exceeded", () => {
    test("Length < maxLength: n = 1", () => {
        const items = ["user1", "user2", "user3"];
        const expectedResult = ["user1 user2 user3"];

        expect(joinMax(items, 100)).toEqual(expectedResult);
    });

    test("Length === maxLength: n = 1", () => {
        const items = ["user1", "user2", "user3"];
        const expectedResult = ["user1 user2 user3"];

        expect(joinMax(items, 17)).toEqual(expectedResult);
    });

    test("Length > maxLength: n > 1", () => {
        const items = ["user1", "user2", "user3"];
        const resultLength5 = ["user1", "user2", "user3"];
        const resultLength6 = resultLength5;

        expect(joinMax(items, 5)).toEqual(resultLength5);
        expect(joinMax(items, 6)).toEqual(resultLength6);
    });
});
