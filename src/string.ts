// Concats all items in @items up to a certain length. If that length is
// reached, then a new "line" will be started
export function joinMax(items: string[],
    maxLength: number,
    delim: string = " "): string[] {
    let ret: string[] = [];
    let tmp = "";

    items.forEach((item) => {
        const concatStr = `${tmp}${delim}${item}`;

        if (concatStr.length > maxLength) {
            // Prevent the function from adding an
            // empty string to the array
            if (tmp.length !== 0)
                ret = ret.concat([tmp]);
            tmp = item;
        } else {
            // Only prepend the delimiter if item
            // is not the first of the string
            tmp = tmp.length === 0 ? `${item}` : concatStr;
        }
    });

    ret = ret.concat([tmp]);
    return ret;
}
