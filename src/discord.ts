import { Logger } from "winston";

import { TextChannel, Client, Message, MessageMentions } from "discord.js";

import { ClientMapping } from "./cmd/types";
import { IClientMetadata, IChannelMember, IIRCClient, ircNickname } from "./irc";

export function isStatusValid(status: string, showOffline: boolean): boolean {
    // These three are always valid
    if (status in ["online", "idle", "dnd"])
        return true;

    // An "offline" status should only be valid when we want
    // those people to appear.
    if (status === "offline")
        return showOffline;
}

// Fetches all text channels and adds them to metadata.discordChannels
export function fetchDiscordChannels(discordClient: Client,
    metadata: IClientMetadata) {
    const { discordID } = metadata;

    discordClient.guilds.get(discordID).channels.array().forEach((channel) => {
        if (channel.type !== "text") return;

        metadata.discordChannels = metadata.discordChannels.concat([{
            id: channel.id,
            discordName: channel.name,
            ircName: `#${channel.name}`,
            members: [], // This will be filled in when we join
        }]);
    });
}

function memberFromID(ircClient: IIRCClient, id: string): IChannelMember | undefined {
    const { metadata } = ircClient;
    const activeChannels = metadata.discordChannels.filter((channel) => {
        // Only process the active channels
        return metadata.activeChannels.includes(channel.ircName);
    });

    // Now try to find the member
    // NOTE: We use two nested for-loops as we may need to exit early
    for (let channel of activeChannels) {
        for (let member of channel.members) {
            if (member.id === id) return member;
        }
    }

    // No match
    return undefined;
}

// Prepare a message for use with IRC by resolving all mentions
export function discordMessageToIRC(ircClients: ClientMapping,
    discordClient: Client,
    metadata: IClientMetadata,
    logger: Logger,
    message: Message): string {
    // Resolve any user mentions, if we have any
    let msgText = message.content;

    // Check if we have any attachments
    if (message.attachments.array().length > 0) {
        message.attachments.array().forEach(attachment => {
            msgText = `[[ ${attachment.url} ]]\r\n` + msgText;
        });
    }

    // Itterate over all mentions and try to match them with an
    // IRC nickname
    const hasUserMentions = msgText.match(MessageMentions.USERS_PATTERN);
    if (hasUserMentions) {
        logger.debug("Found user mention(s) in the message");

        hasUserMentions.forEach(match => {
            // Remove the <@ and the >
            const id = match.slice(2, -1);

            // See if the client has been mentioned
            if (id === discordClient.user.id) {
                msgText = msgText.replace(match, `@${metadata.iDisplayName}`);
                return;
            }

            // We need to resolve the user mention somehow
            // Try to resolve the mention using the active server list
            // NOTE: We need @client here, because its metadata contains
            //       the list of channels that are known and/or active.
            const client = Object.values(ircClients).find((client) => {
                return client.metadata.discordID === message.guild.id;
            });
            const serverListMatch = memberFromID(client, id);
            if (serverListMatch) {
                // We were able to resolve the ID using the server list
                msgText = msgText.replace(match, `@${serverListMatch.ircNickname}`);
            } else {
                // We need to query the API
                const guildMember = discordClient.guilds.get(message.guild.id)
                    .members.get(id);

                if (!guildMember) {
                    // The user is not on the server. Perhaps he got banned
                    // or left the server?
                    // One last try: Resolve using the mentions attribute
                    const user = message.mentions.users.array().find(u => {
                        return u.id === id;
                    });

                    if (!user) {
                        logger.error(`Failed to resolve the username of the ID "${id}"`);
                        return;
                    } else {
                        // TODO: Maybe send a bot message in this case?
                        msgText = msgText.replace(match, `@${user.username}[Not Found]`);
                    }
                } else {
                    // Calculate the IRC name and perform some replacement magic
                    const { displayName, user } = guildMember;
                    const { bot, discriminator } = user;
                    const ircNick = ircNickname(displayName, discriminator, bot);

                    msgText = msgText.replace(match, `@{ircNick}`);
                }
            }
        });
    }

    const hasRoleMentions = msgText.match(MessageMentions.ROLES_PATTERN);
    if (hasRoleMentions) {
        hasRoleMentions.forEach(match => {
            // Remove the <@& and the >
            const id = match.slice(3, -1);
            logger.debug(`Trying to resolve role mention "${match}" => "${id}"`);

            const roles = (message.channel as TextChannel).guild.roles.array();
            const role = roles.find(r => {
                logger.debug(`Checking role ${r.name} (${r.id})`);
                return r.id === id
            });

            if (role) {
                logger.debug(`Resolved ${match} to "@&{role.name}"`);
                msgText = msgText.replace(match, `@&${role.name}`);
            }
        });
    }

    return msgText;
};
